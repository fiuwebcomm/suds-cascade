====================
Suds-Cascade Project
====================

Description
===========

This library builds on the `Suds SOAP Client` <https://fedorahosted.org/suds/>_ to
create a client for the `Cascade CMS` <http://www.hannonhill.com/products/index.html>.
